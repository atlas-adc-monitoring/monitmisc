#!/bin/bash
KRB5CCNAME=$(mktemp /tmp/krb5cc_$(id -u)_XXXXXX)
export KRB5CCNAME
trap "{ rm -f $KRB5CCNAME; }" EXIT
kinit -k -t /data/atlcrc/keytab atlcrc@CERN.CH
#
python3 $1/pandaqueuestatus.py
