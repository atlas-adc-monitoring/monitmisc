#! /usr/bin/python3
import os
#import re
import sys
import requests
import json
import time
from datetime import datetime, timedelta, timezone
import logging
import pprint

if os.path.exists('/data/atlcrc/'):
  pandaqueues = '/cvmfs/atlas.cern.ch/repo/sw/local/etc/cric_pandaqueues.json'
  logging.basicConfig(filename='/eos/home-a/atlcrc/www/other/pandaqueuestatus.log', filemode='w', format='%(message)s', level=logging.INFO)
else:
  pandaqueues = 'cric_pandaqueues.json'
  logging.basicConfig(filename='pandaqueuestatus.log', filemode='w', format='%(message)s', level=logging.INFO)

logging.info("extraction script started: "+str(datetime.now()))

#get list of active queues
with open(pandaqueues) as pq:
    pqjson = json.load(pq)

statuscode = {}
statuscode['online'] = 1
statuscode['test'] = 2
statuscode['brokeroff'] = 3
statuscode['offline'] = 4
statuscode['paused'] = 5

monitreport = []
nowtimestamp = int(datetime.now(tz=timezone.utc).timestamp())*1000
for i in pqjson:
  #print(pqjson[i]['state'])
  if pqjson[i]['state'] == 'ACTIVE':
    #print(pqjson[i]['status'])
    #print(f"{i}: {pqjson[i]['status']}")
    report = {}
    report['producer'] = 'pandaqueuestatus'
    report['type'] = 'status'
    report['timestamp'] = nowtimestamp#timestamp in miliseconds
    report['site'] = pqjson[i]['atlas_site']
    report['queue'] = i
    report['queue_status'] = pqjson[i]['status']
    report['queue_status_code'] = statuscode[pqjson[i]['status']]
    report['_id'] = report['queue']+'_'+str(report['timestamp'])#Each document is expected to come with a different _id
    monitreport.append(report)
#pprint.pprint(monitreport)
#print(len(monitreport))
logging.info(monitreport)

#sending to monit metrics - max length is 10000 messages
sendCounter = 1
sendLimit = 5000
sendDict = {}
sendDict[1] = []
for i in range(0, len(monitreport)):
  if i >= sendLimit*sendCounter:
    sendCounter += 1
    sendDict[sendCounter] = []
  sendDict[sendCounter].append(monitreport[i])
#pprint.pprint(sendDict)
if os.path.exists('/data/atlcrc/'):
  for s in list(sendDict):
    response = requests.post('http://monit-metrics:10012/', data=json.dumps(sendDict[s]), headers={ "Content-Type": "application/json; charset=UTF-8"})
    logging.info(sendDict[s])
    logging.info("response status code: "+str(response.status_code))
    if response.status_code != 200:
      logging.error(response.text)
    time.sleep(5)

logging.info("extraction script finished: "+str(datetime.now()))
